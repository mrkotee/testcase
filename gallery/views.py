from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, QueryDict, Http404
from testcase.alchemy import session
from usermanage.models import User
from .models import Album, Photo
from testcase.tasks import image_chain

from pymemcache.client.base import Client
import uuid


@csrf_exempt
def albums(request, id=None, album_id=None):

    try:
        session_user = session.query(User).get(request.session['member_id'])
    except KeyError:
        session_user = None

    if session_user:
        if id:
            user = session.query(User).get(id)

        if request.method == "GET":
            if not album_id:
                if not id:
                    all_albums = session.query(Album).all()
                    album_list = [album.get_str()+"<br>" for album in all_albums]
                    # for album in user_albums:
                    #     album_list.append(album.get_str() + '\n')
                    return HttpResponse(album_list)

                user_albums = user.albums

                album_list = []
                for album in user_albums:
                    album_list.append(album.get_str() + ' id: ' + str(album.id) + '<br>')

                return HttpResponse(album_list)

            album = session.query(Album).get(album_id)

            if album:

                return HttpResponse(album.get_str())

            else:

                return HttpResponse("Album not found!")

        elif request.method == "POST":
            if not album_id:
                title = request.POST.get('title')
                description = request.POST.get('description')

                new_album = Album(title=title, description=description, user_id=str(session_user.id))

                session.add(new_album)
                session.commit()
                url = '/users/%s/albums/%s' % (session_user.id, new_album.id)
                return HttpResponseRedirect(url)
                # return HttpResponse('New album created')

            else:
                return HttpResponse('Wrong method')

        elif request.method == "PUT":
            if not album_id:
                return HttpResponse('Need album id')

            album = session.query(Album).get(album_id)

            if not album:
                return HttpResponse('Album does not exist')

            elif request.session['member_id'] != album.user_id:
                return HttpResponse('You have no power here!')

            PUT = QueryDict(request.body)

            title = PUT.get('title')
            description = PUT.get('description')

            if title:
                album.title = title

            if description:
                album.description = description

            session.add(album)
            session.commit()
            url = '/users/%s/albums/%s' % (session_user.id, album.id)
            return HttpResponseRedirect(url)

        elif request.method == "DELETE":

            if not album_id:
                return HttpResponse('Need album id')

            album = session.query(Album).get(album_id)

            if not album:
                return HttpResponse('Album does not exist')

            elif request.session['member_id'] != album.user_id:
                return HttpResponse('You have no power here!')

            session.delete(album)
            session.commit()

            url = '/users/%s/albums/' % session_user.id
            return HttpResponseRedirect(url)

    else:
        return HttpResponseRedirect('/login/')


@csrf_exempt
def photos(request, id=None, album_id=None, photo_id=None):

    try:
        session_user = session.query(User).get(request.session['member_id'])
    except KeyError:
        session_user = None

    if session_user:
        if id:
            user = session.query(User).get(id)

        if request.method == "GET":
            if not photo_id:
                if not album_id:
                    if not id:
                        raise Http404

                    user_photos = user.photos

                    photo_list = []
                    for photo in user_photos:
                        photo_str = "Title: {}<br>ID: {}<br><img src={}>".format(
                                                                            photo.title,
                                                                            photo.id,
                                                                            photo.get_thumbnail_rel_path()
                                                                        )
                        photo_list.append(photo_str)

                    return HttpResponse(photo_list)

                album = session.query(Album).get(album_id)

                if album:
                    album_photos = ["<img src={}>".format(photo.get_thumbnail_rel_path()) for photo in album.photos]
                    return HttpResponse(album_photos)

                else:
                    return HttpResponse("Album not found!")

            photo = session.query(Photo).get(photo_id)

            if photo:
                return HttpResponse("<img src={}>".format(photo.get_rel_path()))

            else:
                return HttpResponse("Photo not found!")

        elif request.method == "POST":
            if not photo_id:
                if album_id:
                    image = request.FILES['image'].read()
                    title = request.POST.get('title')
                    key = str(uuid.uuid4())

                    client = Client(('localhost', 11211))

                    client.set(key, image)

                    album_url = request.build_absolute_uri()

                    image_chain.delay(request_image=key,
                                      album_id=album_id,
                                      title=title,
                                      user_id=session_user.id,
                                      album_url=album_url,
                                      album_title=session.query(Album).get(album_id).title)

                    url = '/users/%s/albums/%s/photos/' % (session_user.id, album_id)
                    return HttpResponseRedirect(url)

                else:
                    return HttpResponse("Need album id")

            else:
                return HttpResponse('Wrong method')

        elif request.method == "PUT":

            if not photo_id:
                return HttpResponse('Need photo id')

            photo = session.query(Photo).get(photo_id)

            if not photo:
                return HttpResponse('Photo does not exist')

            elif request.session['member_id'] != photo.user_id:
                return HttpResponse('You have no power here!')

            PUT = QueryDict(request.body)

            title = PUT.get('title')
            album_id = PUT.get('album_id')

            if title:
                photo.title = title

            if album_id:
                photo.album_id = album_id

            session.add(photo)
            session.commit()
            url = '/users/%s/albums/%s/photo/%s' % (session_user.id, photo.album_id, photo.id)
            return HttpResponseRedirect(url)

        elif request.method == "DELETE":

            if not photo_id:
                return HttpResponse('Need photo id')

            photo = session.query(Photo).get(photo_id)
            album_id = photo.album_id

            if not photo:
                return HttpResponse('Photo does not exist')

            elif request.session['member_id'] != photo.user_id:
                return HttpResponse('You have no power here!')

            session.delete(photo)
            session.commit()

            url = '/users/%s/albums/%s' % (session_user.id, album_id)
            return HttpResponseRedirect(url)

    else:
        return HttpResponseRedirect('/login/')

