from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import String, Date, DateTime, Boolean, LargeBinary
from sqlalchemy_utils import EmailType, ChoiceType, PasswordType
from sqlalchemy.dialects.postgresql import UUID
from PIL import Image
from io import BytesIO
from datetime import datetime as dt
import uuid
import os
try:
    from testcase.alchemy import Base, session
    from usermanage.models import User
    from testcase import settings
except ImportError:
    from testcase.testcase.alchemy import Base, session
    from testcase.usermanage.models import User
    from testcase.testcase import settings


class Album(Base):
    __tablename__ = "albums"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = Column("title", String(40))
    description = Column("description", String(140), nullable=True)
    user_id = Column(UUID, ForeignKey("users.id"))
    user = relationship(User, backref="albums")


    def get_str(self):
        return "title: %s, description: %s" % (self.title, self.description)


class Photo(Base):
    __tablename__ = "photos"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    album_id = Column(UUID, ForeignKey("albums.id"))
    user_id = Column(UUID, ForeignKey("users.id"))
    album = relationship(Album, backref="photos")
    user = relationship(User, backref="photos")
    title = Column("title", String(50), nullable=True)
    _img_path = Column("image rel path", String(256))
    date_added = Column('date added', DateTime, default=dt.utcnow)

    def __init__(self, pil_image, album_id, user_id=None, title=None):
        self.id = str(uuid.uuid4())
        self.set_image(pil_image)
        self.album_id = album_id

        if user_id:
            self.user_id = user_id
        else:
            self.user_id = session.query(Album).get(album_id).user_id

        if title:
            self.title = title

    def set_image(self, p_image):
        # img = open('testcase/media/images/testphoto', 'rb').read()
        # pimg = Image.open(BytesIO(img))

        # im = Image.open('input.jpg')
        # im.thumbnail((64, 64))
        # im.save("thumbnail.jpg", "JPEG")

        # img = Image.open(p_image)
        imgfile = str(self.id) + '.' + p_image.format
        thumbfile = str(self.id) + '-thumbnail.' + p_image.format
        imgpath = os.path.join('images', imgfile)
        path = os.path.join(settings.MEDIA_ROOT, imgpath)
        p_image.save(path)
        p_image.thumbnail([512, 512], Image.ANTIALIAS)
        p_image.save(os.path.join(settings.MEDIA_ROOT, 'images', thumbfile))
        # open(path, 'wb').write(p_image)
        self._img_path = '/media/' + imgpath

    def get_rel_path(self):
        return self._img_path

    def get_abs_path(self):
        path = os.path.join(settings.MEDIA_ROOT, self._img_path)
        return path

    def get_thumbnail_rel_path(self):
        rel_path = '.'.join(self._img_path.split('.')[:-1]) + '-thumbnail.' + self._img_path.split('.')[-1]
        return rel_path



if __name__ == "__main__":
    # from testcase.testcase.alchemy import con

    Base.metadata.create_all()
