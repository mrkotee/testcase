from django.urls import path, re_path
from .views import albums, photos


urlpatterns = [
    path('', albums),
    path('<uuid:album_id>', albums),
    path('<uuid:album_id>/photos/', photos),
    path('<uuid:album_id>/photos/<uuid:photo_id>', photos),
]