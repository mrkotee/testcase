from django.urls import path, re_path
from .views import users


urlpatterns = [
    path('', users),
    path('<uuid:id>', users)
]