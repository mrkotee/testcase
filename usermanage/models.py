from sqlalchemy.schema import Column
from sqlalchemy.types import String, Date, DateTime, Boolean
from sqlalchemy_utils import EmailType, ChoiceType, PasswordType
from sqlalchemy.dialects.postgresql import UUID
from datetime import datetime as dt
import uuid
try:
    from testcase.alchemy import Base
    from usermanage.catalog.eyes_colors import EYES_COLORS
    from usermanage.catalog.countries import COUNTRIES
except ImportError:
    from testcase.testcase.alchemy import Base
    from testcase.usermanage.catalog.eyes_colors import EYES_COLORS
    from testcase.usermanage.catalog.countries import COUNTRIES


class User(Base):
    __tablename__ = "users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    email = Column(EmailType, unique=True)
    password = Column(PasswordType(
        schemes=[
            'pbkdf2_sha512',
            'md5_crypt'
        ],

        deprecated=['md5_crypt']
    ))
    first_name = Column('first name', String(30), nullable=True)
    middle_name = Column('middle name', String(30), nullable=True)
    last_name = Column('last name', String(30), nullable=True)
    birthdate = Column('birthdate', Date, nullable=True)
    eyes_color = Column(ChoiceType(EYES_COLORS), nullable=True)
    country = Column(ChoiceType(COUNTRIES), nullable=True)
    date_joined = Column('date joined', DateTime, default=dt.utcnow)

    def __init__(self, email, password):
        self.email = email
        self.password = password
        # for key, value in kwargs.items():
        #     self[key] = value

    # def edit(self, email=None, password=None, first_name=None, middle_name=None, last_name=None, birthdate=None, eyes_color=None, country=None):
    def edit(self, **kwargs):
        if 'email' in kwargs:
            self.email = kwargs['email']
        if 'password' in kwargs:
            self.password = kwargs['password']
        if 'first_name' in kwargs:
            self.first_name = kwargs['first_name']
        if 'middle_name' in kwargs:
            self.middle_name = kwargs['middle_name']
        if 'last_name' in kwargs:
            self.last_name = kwargs['last_name']
        if 'birthdate' in kwargs:
            self.birthdate = kwargs['birthdate']
        if 'eyes_color' in kwargs:
            self.eyes_color = kwargs['eyes_color']
        if 'country' in kwargs:
            self.country = kwargs['country']

    def get_str(self):
        return "id: {}, email: {}, name: {} {}".format(self.id,
                                                       self.email,
                                                       self.first_name,
                                                       self.last_name)

    def __srt__(self):
        return "id: {}, email: {}, name: {} {}".format(self.id,
                                                       self.email,
                                                       self.first_name,
                                                       self.last_name)


if __name__ == "__main__":
    # from testcase.testcase.alchemy import con

    Base.metadata.create_all()
