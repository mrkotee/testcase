

EYES_COLORS = (
    ('DBL', 'Dark Blue'),
    ('BL', 'Blue'),
    ('GRY', 'Grey'),
    ('GRN', 'Green'),
    ('AMB', 'Amber'),
    ('OLV', 'Olive'),
    ('BRN', 'Brown'),
    ('BLK', 'Black'),
    ('YLW', 'Yellow'),
)