from django.shortcuts import render
# from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, QueryDict
from testcase.alchemy import session
from .models import User


@csrf_exempt
def auth(request):
    if request.method == "POST":

        email = request.POST['email']
        password = request.POST['password']

        user = session.query(User).filter(User.email == email).first()
        if user:
            if user.password == password:
                request.session['member_id'] = str(user.id)

                url = "/users/%s" % str(user.id)
                return HttpResponseRedirect(url)
                # return HttpResponse("login success")
                # try:
                #     _next = request.POST.get('next')
                # except Exception as e:
                #     print(e)
                # return HttpResponseRedirect(_next)

            else:
                return HttpResponse("Invalid password")

        else:
            return HttpResponse("Invalid login")

    elif request.method == "GET":
        _next = request.GET.get('next')
        return render(request, 'login.html', {'next': _next})


# @login_required(login_url="/login/")
@csrf_exempt
def users(request, id=None):

    try:
        session_user = session.query(User).get(request.session['member_id'])
    except KeyError:
        session_user = None

    if session_user:
        user_list = ['first_name', 'middle_name', 'last_name', 'birthdate', 'eyes_color', 'country']

        if request.method == "GET":
            if not id:
                user_query = session.query(User).all()

                user_list = []
                for user in user_query:
                    user_list.append(user.get_str() + '\n')

                return HttpResponse(user_list)

            user = session.query(User).get(id)

            if user:

                return HttpResponse(user.get_str())

            else:

                return HttpResponse("User not found!")

        elif request.method == "POST":
            if not id:
                email = request.POST.get('email')

                if session.query(User).filter(User.email == email).first():
                    return HttpResponse('This email already registered')

                password = request.POST.get('password')

                user = User(email=email, password=password)
                for field in user_list:
                    value = request.POST.get(field)
                    if value:
                        user.__dict__[field] = value

                session.add(user)
                session.commit()

                url = '/users/%s' % user.id
                return HttpResponseRedirect(url)

            else:
                HttpResponse('Wrong method')

        elif request.method == "PUT":
            if not id:
                return HttpResponse('Need id')

            user = session.query(User).get(id)

            if not user:
                return HttpResponse('User does not exist')

            elif request.session['member_id'] != id:
                return HttpResponse('You have no power here!')

            PUT = QueryDict(request.body)

            email = PUT.get('email')
            password = PUT.get('password')
            # first_name = PUT.get('first_name')
            # middle_name = PUT.get('middle_name')
            # last_name = PUT.get('last_name')
            # birthdate = PUT.get('birthdate')
            # eyes_color = PUT.get('eyes_color')
            # country = PUT.get('country')

            if email:
                if session.query(User).filter(User.email == email).first():
                    return HttpResponse('This email already registered')
                user.email = email

            if password:
                user.password = password

            user_dict = {}
            for field in user_list:
                value = PUT.get(field)
                if value:
                    user_dict[field] = value

            user.edit(**user_dict)

            session.add(user)
            session.commit()
            url = '/users/%s' % user.id
            return HttpResponseRedirect(url)

    else:
        # path = request.build_absolute_uri()
        # url = "%s?next=%s" % ('/login/', path)
        return HttpResponseRedirect('/login/')


