from .celery import app
from celery import chain
try:
    from .alchemy import new_session
    from usermanage.models import User
    from gallery.models import Album, Photo
except ImportError:
    from testcase.testcase.alchemy import new_session
    from testcase.usermanage.models import User
    from testcase.gallery.models import Album, Photo
from PIL import Image
from io import BytesIO
from django.core.mail import send_mail
from pymemcache.client.base import Client


@app.task()
def load_image(*args, **kwargs):
    image_key = kwargs['request_image']
    album_id = kwargs['album_id']
    title = kwargs['title']

    client = Client(('localhost', 11211))

    b_image = client.get(image_key)
    client.delete(image_key)

    session = new_session()

    p_image = Image.open(BytesIO(b_image))
    new_photo = Photo(pil_image=p_image, album_id=str(album_id), title=title)

    session.add(new_photo)
    session.commit()
    session.close()
    del(session)

    return kwargs['user_id'], kwargs['album_url'], kwargs['album_title']


@app.task(ignore_result=True)
def notify_user(*args):

    user_id, album_url, album_title = args[0]

    session = new_session()
    email = session.query(User).get(str(user_id)).email
    message = 'New photo in album {}, url: {}'.format(album_title, album_url)

    try:
        send_mail('New photo!', message, 'from@testcase.com',
            [email], fail_silently=False)
    except ConnectionRefusedError:
        print('email not send')


image_chain = chain(load_image.s(), notify_user.s())
