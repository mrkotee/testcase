from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
try:
    from testcase import settings
except ImportError:
    from testcase.testcase import settings


def connect(user, password, db, host='localhost', port=5432):
    db_url = "postgresql+psycopg2://{}:{}@{}:{}/{}".format(user, password, host, port, db)
    # engine = create_engine("postgresql+psycopg2://alchemy:qwerty@localhost:5432/testcase")
    engine = create_engine(db_url)
    connection = engine.connect()

    meta = MetaData(bind=connection)  # , reflect=True

    return connection, meta


def new_session():
    user = settings.DATABASES['default']['USER']
    password = settings.DATABASES['default']['PASSWORD']
    db = settings.DATABASES['default']['NAME']
    host = settings.DATABASES['default']['HOST']
    port = settings.DATABASES['default']['PORT']
    db_url = "postgresql+psycopg2://{}:{}@{}:{}/{}".format(user, password, host, port, db)
    engine = create_engine(db_url)
    con = engine.connect()
    Session = sessionmaker()
    session = Session(bind=con)
    return session


# if __name__ == "__main__":
user = settings.DATABASES['default']['USER']
password = settings.DATABASES['default']['PASSWORD']
db = settings.DATABASES['default']['NAME']
host = settings.DATABASES['default']['HOST']
port = settings.DATABASES['default']['PORT']

con, meta = connect(user, password, db, host, port)

Base = declarative_base(bind=con, metadata=meta)

Session = sessionmaker()
session = Session(bind=con)


